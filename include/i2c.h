
#include "avr/io.h"
//#include "avr/io8515.h"
#include "util/delay.h"

// I2C — последовательная шина данных для связи интегральных схем, 
// использующая две двунаправленные линии связи (SDA и SCL). 
// Используется для соединения низкоскоростных периферийных компонентов 
// с материнской платой, встраиваемыми системами и мобильными телефонами.
// Для начала нам понадобиться много #define'ов

///i2c port 
#define i2c_time 10 // пауза в микросекундах (4 паузы на такт передачи)
#define i2c_port PORTB 	
#define i2c_ddr DDRB	
#define i2c_pin PINB
#define sda 2	       // номера выводов соответствующего порта
#define scl 3	
// управление битами при записи
#define clrsda i2c_port &= ~(1 << sda)
#define setsda i2c_port |= (1 << sda)
#define clrscl i2c_port &= ~(1 << scl)
#define setscl i2c_port |= (1 << scl) 
// управление битами при чтении
#define rxscl i2c_ddr &= ~(1 << scl) 
#define txscl i2c_ddr |= (1 << scl) 
#define txsda i2c_ddr |= (1 << sda)
#define rxsda i2c_ddr &= ~(1 << sda) 

#define i2c_readstatus (1 << sda)


// Для работы потребуется функция осуществляющую задержку. 
// Если вы используете IAR то её объявление выглядит так:

#ifndef DELAY_H
#define DELAY_H
#endif
#define CPU_CLK 8000000 //частота на которой работает контроллер
// #define delay_us(u) __delay_cycles((CPU_CLK/1000000)*u)
// #define delay_ms(m) __delay_cycles((CPU_CLK/1000)*m)
#define delay_us(u) _delay_us(u)//???
#define delay_ms(m) _delay_ms(m)//???


void loadtoTDA(void);